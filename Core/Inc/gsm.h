#ifndef INC_GSM_H_
#define INC_GSM_H_

#include <stm32l4xx_hal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "pwr.h"

#define BUFFER_SIZE 1024

/*
 * Buffer for transmitting and receiving UART data
 * It needs to be external. This is how the UART can handle it properly
 * Size of BUFFER_SIZE is optional. If it's big enough the data
 */
extern uint8_t rx_buffer[BUFFER_SIZE];

/*
 * Initialize gsm module.
 */
bool init_gsm(UART_HandleTypeDef *huart);

/*
 * When the work is finished the gsm connection need to be closed.
 */
bool close_gsm(UART_HandleTypeDef *huart);

/*
 * The datas are sending over gsm connection to the server.
 * @param data  This buffer contains the data to be send.
 */
bool send_data(UART_HandleTypeDef *huart, uint8_t* data);

/*
 * Request to the server. The returned data must contains the actual date.
 * @param date_buffer  The returned actual date put there. This will be send to measurement points over LoRa communication.
 */
bool set_date(UART_HandleTypeDef *huart, RTC_HandleTypeDef *rtc, char* date_buffer);

/*
 * Request to the server. The returned data must contains the actual date.
 * ----------------------------------------------------
 * Deprecated. This function moved to set_date function.
 * ----------------------------------------------------
 */
bool set_time(UART_HandleTypeDef *huart, RTC_HandleTypeDef *rtc);


/*
 * This function can reset the gsm module if required.
 */
void gsm_reset();

/*
 * Request to the server. The returned data must be the scale ids.
 * @param data  This will be the buffer of the scale ids.
 */
bool get_scale_ids(UART_HandleTypeDef *huart, char* data);

#endif /* INC_GSM_H_ */
