
#ifndef INC_PWR_H_
#define INC_PWR_H_

#include <string.h>
#include <stdlib.h>
#include <stm32l4xx_hal.h>
#include <stdbool.h>
#include <cJSON.h>
#include <main.h>

/*
 * Set self RTC date.
 * @param date_buffer  This buffer contains the date and the time.
 */
bool set_rtc_date_time(char* date, RTC_HandleTypeDef *rtc, char* date_buffer);


//  The wake-up time need to set, all peripherals must be turned off, mcu must be set to sleep mode.

void go_to_sleep(RTC_HandleTypeDef *rtc, RTC_AlarmTypeDef sAlarm);

#endif /* INC_PWR_H_ */
