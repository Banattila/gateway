#include "pwr.h"

// This function setup the date and time of the self RTC.
bool set_rtc_date_time(char* date, RTC_HandleTypeDef *rtc, char* date_buffer)
{
	cJSON *json = cJSON_Parse(date);
	cJSON *year;
	cJSON *month;
	cJSON *day;
	cJSON *hour;
	cJSON *min;
	cJSON *sec;
	RTC_DateTypeDef s_date;
	RTC_TimeTypeDef s_time;

	if(json != NULL)
	{
		date_buffer = strcpy(date_buffer, date);
		year = cJSON_GetObjectItem(json, "year");
		month = cJSON_GetObjectItem(json, "month");
		day = cJSON_GetObjectItem(json, "day");
		hour = cJSON_GetObjectItem(json, "hour");
		min = cJSON_GetObjectItem(json, "min");
		sec = cJSON_GetObjectItem(json, "sec");

		s_date.Month = month->valueint;
		s_date.Year = year->valueint;
		s_date.Date = day->valueint;

		//This parameter initialize with 0 because this will be easier solution. This don't need to increment all day.
		s_date.WeekDay = 0;
		s_time.Hours = hour->valueint;
		s_time.Minutes = min->valueint;
		s_time.Seconds = sec->valueint;
		s_time.TimeFormat = RTC_HOURFORMAT_24;

		// All cJson objects need to free in the memory.
		free(year);
		free(month);
		free(day);
		free(hour);
		free(min);
		free(sec);
		free(json);
	} else {
		return false;
	}

	if(HAL_RTC_SetDate(rtc, &s_date, RTC_FORMAT_BCD) != HAL_OK){

		return false;
	}

	if(HAL_RTC_SetTime(rtc, &s_time, RTC_FORMAT_BCD) != HAL_OK){
			return false;
		}

	return true;
}


// This function help to enter to the standby mode.
void go_to_sleep(RTC_HandleTypeDef *rtc, RTC_AlarmTypeDef sAlarm)
{
	// The alarm is setting up.
	if (HAL_RTC_SetAlarm_IT(rtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK) {
	  Error_Handler();
	}

	// The Standby mode is starting.
	HAL_SuspendTick();
	HAL_RTC_SetAlarm_IT(rtc, &sAlarm, RTC_FORMAT_BIN);
	HAL_PWR_EnterSTANDBYMode();
}
