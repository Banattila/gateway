/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "rtc.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "gsm.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "LoRa.h"
#include <cJSON.h>
#include <pwr.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t rx_buffer[BUFFER_SIZE];
uint8_t error_counting = 0;
static const int LORA_BUFFER_SIZE = 100;
RTC_AlarmTypeDef sAlarm;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// This function initialize the GSM module and it send a request to the server for the scale ids and actual date.
bool gsm_initialize(char *date_buffer, char *scale_ids) {
	bool gsm_init_result;
	gsm_init_result = init_gsm(&huart1);
	HAL_Delay(100);
	if (gsm_init_result) {
		set_date(&huart1, &hrtc, date_buffer);
		HAL_Delay(100);
		get_scale_ids(&huart1, scale_ids);
		return true;
	} else {
		return false;
	}
}

// The time of the alarm need to setting up in this function.
void set_alarm_date_time(RTC_AlarmTypeDef *sAlarm) {
	RTC_TimeTypeDef s_time;
	HAL_RTC_GetTime(&hrtc, &s_time, RTC_FORMAT_BCD);
	if (s_time.Minutes >= 60) {
		s_time.Minutes -= 60;
		s_time.Hours += 1;
		if (s_time.Hours >= 24) {
			s_time.Hours = 0;
		}
	}
	sAlarm->AlarmTime = s_time;
	sAlarm->AlarmTime.Hours = 21;
	sAlarm->AlarmTime.Minutes = 0;
	sAlarm->Alarm = RTC_ALARM_A;
	sAlarm->AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
	sAlarm->AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
	sAlarm->AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	//This need to setup for 1. It's presenting the next day.
	sAlarm->AlarmDateWeekDay = 1;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

	uint32_t packet_size = 0;

	char lora_buffer[LORA_BUFFER_SIZE];
	char scale_ids[BUFFER_SIZE];
	cJSON *scales;
	LoRa myLoRa;

	gsm_initialize(lora_buffer, scale_ids);

	myLoRa = newLoRa();
	myLoRa.CS_port = NSS_GPIO_Port;
	myLoRa.CS_pin = NSS_Pin;
	myLoRa.reset_port = RESET_GPIO_Port;
	myLoRa.reset_pin = RESET_Pin;
	myLoRa.DIO0_port = DIO0_GPIO_Port;
	myLoRa.DIO0_pin = DIO0_Pin;
	myLoRa.hSPIx = &hspi1;

	myLoRa.frequency = 433;
	myLoRa.spredingFactor = SF_7;
	myLoRa.bandWidth = BW_125KHz;
	myLoRa.crcRate = CR_4_5;
	myLoRa.power = POWER_17db;
	myLoRa.overCurrentProtection = 130;
	myLoRa.preamble = 9;

	LoRa_init(&myLoRa);
	HAL_Delay(1000);
	LoRa_transmit(&myLoRa, (uint8_t*) lora_buffer, LORA_BUFFER_SIZE, 10000);
	HAL_Delay(10000);

	scales = cJSON_Parse(scale_ids);

	if (scales != NULL && cJSON_IsArray(scales)) {
		uint8_t wait_counter = 0;
		cJSON *scale;
		cJSON *scale_id;
		cJSON_ArrayForEach(scale, scales)
		{
			scale_id = cJSON_GetObjectItem(scale, "scale_id");
			memset(lora_buffer, '\0', LORA_BUFFER_SIZE);
			strcpy(lora_buffer, scale_id->valuestring);
			LoRa_transmit(&myLoRa, (uint8_t*) lora_buffer, LORA_BUFFER_SIZE,
					10000);
			HAL_Delay(10000);
			LoRa_startReceiving(&myLoRa);
			HAL_Delay(100);
			while (packet_size <= 1 && wait_counter < 5) {
				packet_size = LoRa_receive(&myLoRa, (uint8_t*) lora_buffer,
						LORA_BUFFER_SIZE);
				HAL_Delay(10000);
				wait_counter++;
			}
			send_data(&huart1, (uint8_t*) lora_buffer);
			HAL_Delay(1000);
		}
	}
	set_alarm_date_time(&sAlarm);
	close_gsm(&huart1);
	LoRa_gotoMode(&myLoRa, SLEEP_MODE);
	go_to_sleep(&hrtc, sAlarm);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	memset(rx_buffer, 0, BUFFER_SIZE);
	HAL_UART_Receive_IT(huart, rx_buffer, BUFFER_SIZE);
}

//void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
//{
//	HAL_PWR_EnableBkUpAccess();
//	WRITE_REG(RTC->SCR, RTC_SCR_CALRAF);
//	HAL_PWR_DisableBkUpAccess();
//}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
