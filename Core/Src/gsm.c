#include <gsm.h>
#include "main.h"
#include "pwr.h"


void clear_buffers()
{
	memset(rx_buffer, 0, BUFFER_SIZE);
}

void send_cmd_it(UART_HandleTypeDef *huart, char* command, uint32_t delay)
{

	while((huart->gState != HAL_UART_STATE_READY));
	clear_buffers();
	huart->pRxBuffPtr = rx_buffer;
	huart->RxXferCount = BUFFER_SIZE;
	HAL_Delay(500);
	HAL_UART_Transmit_IT(huart, (uint8_t*) command, strlen(command));
	HAL_UART_Receive_IT(huart, rx_buffer, BUFFER_SIZE);
	HAL_Delay(delay);
}


bool init_gsm(UART_HandleTypeDef *huart)
{
	bool result = 0;
	send_cmd_it(huart, "AT+CSCLK=0\r\n", 1000);
	send_cmd_it(huart, "ATE0\r\n", 250);
	send_cmd_it(huart, "AT\r\n", 250);
	send_cmd_it(huart, "AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r\n", 500);
	send_cmd_it(huart, "AT+SAPBR=3,1,\"APN\",\"internet.vodafone.net\"\r\n", 500);
	send_cmd_it(huart, "AT+SAPBR=1,1\r\n", 500);
	send_cmd_it(huart, "AT+SAPBR=2,1\r\n", 500);

	result = strstr((char*) rx_buffer, "\r\nOK\r\n") != NULL;
	return result;
}

bool close_gsm(UART_HandleTypeDef *huart)
{
	bool result = false;

	send_cmd_it(huart, "AT+SAPBR=0,1\r\n", 500);
	send_cmd_it(huart, "AT+CSCLK=1\r\n", 1000);

	if(strstr((char*) rx_buffer, "\r\nOK\r\n") != NULL || strstr((char*) rx_buffer, "\0") != NULL)
	{
		result = true;
	}
	return result;
}

bool send_data(UART_HandleTypeDef *huart, uint8_t* data)
{
	bool result;
	char res[BUFFER_SIZE];

	send_cmd_it(huart, "AT+HTTPINIT\r\n", 500);
	send_cmd_it(huart, "AT+HTTPPARA=\"CID\",1\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPPARA=\"URL\",\"http://68.183.79.202:3000/api/add-data\"\r\n", 500);
	send_cmd_it(huart, "AT+HTTPPARA=\"CONTENT\",\"application/json\"\r\n", 250);
	send_cmd_it(huart, "AT+HTTPDATA=80,2500\r\n", 500);
	send_cmd_it(huart, (char*) data, 2000);
	send_cmd_it(huart, "AT+HTTPACTION=1\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPREAD\r\n", 2000);
	memcpy(res, rx_buffer, BUFFER_SIZE);
	send_cmd_it(huart, "AT+HTTPTERM\r\n", 250);

	result = strstr((char*)res, "\"answer\":\"SUCCESS SAVING\"") != NULL;
	return result;
}

bool get_scale_ids(UART_HandleTypeDef *huart, char* data)
{
	send_cmd_it(huart, "AT+HTTPINIT\r\n", 500);
	send_cmd_it(huart, "AT+HTTPPARA=\"CID\",1\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPPARA=\"URL\",\"http://68.183.79.202:3000/api/scale-ids\"\r\n", 500);
	send_cmd_it(huart, "AT+HTTPACTION=0\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPREAD\r\n", 3000);
	strncpy(data, (char*)rx_buffer + 42,(size_t) BUFFER_SIZE - 1);

	if(strlen(data) != 0)
	{
		return true;
	}
	return false;
}

bool set_date(UART_HandleTypeDef *huart, RTC_HandleTypeDef *rtc, char* date_buffer)
{
	char res[BUFFER_SIZE] = "2023-05-14";
	send_cmd_it(huart, "AT+HTTPINIT\r\n", 500);
	send_cmd_it(huart, "AT+HTTPPARA=\"CID\",1\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPPARA=\"URL\",\"http://68.183.79.202:3000/api/actual-date\"\r\n", 500);
	send_cmd_it(huart, "AT+HTTPACTION=0\r\n", 1000);
	send_cmd_it(huart, "AT+HTTPREAD\r\n", 2000);
	strncpy(res, (char*)rx_buffer + 42, (size_t) BUFFER_SIZE);

	if(strlen(res) != 0)
	{
		return set_rtc_date_time(res, rtc, date_buffer);
	}
	return false;
}
